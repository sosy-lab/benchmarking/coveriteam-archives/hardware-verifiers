# CoVeriTeam Archives for Hardware Verifiers

- abc (v1.01): https://github.com/berkeley-abc/abc/tree/af1de4fa9c89d6ec19ffede45661111e23ddeb8b
- avr: https://github.com/nianzelee/avr/tree/bdbfc83b8bf925f27e83e74b901bb811169ceb4e
- btormc (v3.2.3): https://github.com/Boolector/boolector/tree/6603ed7b8d401f9bf387f32c702e3e938c50924d
- btorsim: https://github.com/Boolector/btor2tools/tree/037f1fa88fb439dca6f648ad48a3463256d69d8b
- pono: https://github.com/stanford-centaur/pono/tree/b620e4edf6fb3cc0044f847b111c3b8f07f41a4d (built with [MathSAT 5.6.11](https://mathsat.fbk.eu/) and [IC3ia 23.05](https://es-static.fbk.eu/people/griggio/ic3ia/))
- rIC3: https://github.com/gipsyh/rIC3/tree/HWMCC24
- super_prove: https://github.com/sterin/super-prove-build/tree/c7a4df4f60b9fe847f673b566c9a4fb4f71f4181 (built with ABC at commit [`806a996b`](https://github.com/berkeley-abc/abc/tree/806a996b88f57d7d307d3e3e528513b66d101f8c))

## License

Please refer to the tool archives for their license information.
